import pygame

from game.characters import Enemy, Archer
from game import win, WIN_HEIGHT, WIN_WIDTH, FRAMERATE

class GameLoop(object):
  def __init__(self):
    self.clock = pygame.time.Clock()
    self.running = True
    self.player = Archer(100, WIN_HEIGHT - 300, 60, 80)
    self.enemy = Enemy(500, WIN_HEIGHT - 300, 60, 80, WIN_WIDTH - 200)
    self.font = pygame.font.SysFont('freeserif', 40, True, True)
    self.run()

  def render(self):
    win.blit(pygame.image.load('assets/background/background.png').convert(), (0,0))
    self.render_text()
    self.render_arrows()
    self.render_player()
    self.enemy.move()
    pygame.display.update()

  def render_text(self):
    text = self.font.render(f'Score: {self.player.score}', 1, (255, 255, 255))
    win.blit(text, (1700, 10))

  def render_player(self):
    self.player.move()
    if self.player.collision(self.enemy):
      self.player.take_damage()

  def render_arrows(self):
    for arrow in self.player.arrows:
      if arrow.collision(self.enemy):
        self.enemy.take_damage()
        self.player.score_one()
        self.player.delete_arrow(arrow)
        continue

      if arrow.x < WIN_WIDTH and arrow.x > 0:
        arrow.move()
      else:
        self.player.delete_arrow(arrow)

  def run(self):
    while self.running:
      self.clock.tick(FRAMERATE)

      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          self.running = False
      
      self.render()

    pygame.quit()

game = GameLoop()