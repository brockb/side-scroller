import pygame

def flip(target):
  new_list = []
  for image in target:
    new_list.append(pygame.transform.flip(image, True, False))

  return new_list

def player_walkright():
  return [pygame.image.load(f'assets/player/walk_{x}.png') for x in range(1, 10)]

def player_walkleft():
  return flip(player_walkright())

def player_idleright():
  return pygame.image.load('assets/player/walk_1.png')

def player_idleleft():
  return pygame.transform.flip(player_idleright(), True, False)

def warrior_walkright():
  return [pygame.image.load(f'assets/enemy/walk_{x}E.png') for x in range(1, 12)]

def warrior_walkleft():
  return flip(warrior_walkright())