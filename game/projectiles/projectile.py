import pygame

from .. import win
from ..polygons import Polygon

class Projectile(Polygon):
  def __init__(self, x, y, radius, color, facing):
    super().__init__(x, y)
    self.radius = radius
    self.color = color
    self.facing = facing
    self.velocity = 16 * facing

  def render(self):
    self.hitbox = (self.x - self.radius, self.y - self.radius, 12, 12)
    pygame.draw.circle(win, self.color, self.position, self.radius)

  def move(self):
    self.x += self.velocity
    self.render()