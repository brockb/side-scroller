class Polygon(object):
  def __init__(self, x, y):
    self.x = x
    self.y = y
    self.hitbox = (0, 0, 0, 0)

  def __init_subclass__(self, *args, **kwargs):
    if not hasattr(self, 'render'):
      raise TypeError("missing render method")

  @property
  def position(self):
    return (self.x, self.y)

  def collision(self, polygon):
    if hasattr(polygon, 'dead') and polygon.dead:
      return False
      
    if self.hitbox[1] < polygon.hitbox[1] + polygon.hitbox[3] and self.hitbox[1] + self.hitbox[3] > polygon.hitbox[1]:
      if self.hitbox[0] + self.hitbox[2] > polygon.hitbox[0] and self.hitbox[0] < polygon.hitbox[0] + polygon.hitbox[2]:
        return True

    return False