import pygame

from . import Character
from game import WIN_WIDTH

class PlayerCharacter(Character):
  def __init__(self, x, y, width, height):
    super().__init__(x, y, width, height)
    self.jump_count = 10
    self.jumping = False
    self.velocity = 10
    self.score = 0
    self.max_health = 2
    self.health = 2
  
  def move_hitbox(self):
    self.hitbox = (self.x + 17, self.y + 11, 28, 52)

  def score_one(self):
    self.score += 1

  def move(self):
    keys = pygame.key.get_pressed()
    mouse1, mouse2, mouse3 = pygame.mouse.get_pressed()

    if mouse1:
      self.attack()

    if (keys[pygame.K_LEFT] or keys[pygame.K_a]) and self.x > self.velocity:
      self.move_left()
    elif (keys[pygame.K_RIGHT] or keys[pygame.K_d]) and self.x < WIN_WIDTH - self.width:
      self.move_right()
    else:
      self.idle()

    if not self.jumping:
      if keys[pygame.K_SPACE]:
        self.jumping = True
        self.idle() # cancel walking animation
    else:
      self.jump()

    if not self.dead:
      self.render()

  def idle(self):
    self.moving_right = False
    self.moving_left = False
    self.walk_count = 0

  def jump(self):
    if self.jump_count >= -10:
      modifier = 1
      if self.jump_count < 0:
        modifier = -1

      self.y -= (self.jump_count ** 2) * 0.5 * modifier
      self.jump_count -= 1
    else:
      self.jumping = False
      self.jump_count = 10