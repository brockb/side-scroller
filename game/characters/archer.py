from time import time as current_time

from .playercharacter import PlayerCharacter
from ..animations import player_walkright, player_walkleft, player_idleright, player_idleleft
from ..projectiles import FACING_LEFT, FACING_RIGHT, Projectile

class Archer(PlayerCharacter):
  def __init__(self, x, y, width, height):
    super().__init__(x, y, width, height)
    self.walk_right = player_walkright()
    self.walk_left = player_walkleft()
    self.idle_right = player_idleright()
    self.idle_left = player_idleleft()
    self.moving_left = False
    self.moving_right = False
    self.arrows = []
    self.last_attack = 0

  def delete_arrow(self, arrow):
    self.arrows.pop(self.arrows.index(arrow))

  def attack(self):
    current_attack = current_time()
    if current_attack - self.last_attack < .5:
      return
    else:
      self.last_attack = current_attack

    if len(self.arrows) < 5:
      arrow = Projectile(
        round(self.x + round(self.width // 2)),
        round(self.y + round(self.height // 2)),
        6,
        (0, 0, 0),
        FACING_RIGHT if (self.facing_right) else FACING_LEFT
      )
      self.arrows.append(arrow)