import pygame

from .. import win, WIN_HEIGHT, WIN_WIDTH
from ..polygons import Polygon

class Character(Polygon):
  def __init__(self, x, y, width, height):
    super().__init__(x, y)
    self.width = width
    self.height = height
    self.facing_right = True
    self.walk_count = 0
    self.dead = False

  def render(self):
    if self.walk_count >= len(self.walk_right):
      self.walk_count = 0

    if self.moving_left:
      self.facing_right = False
      win.blit(self.walk_left[self.walk_count], self.position)
      self.walk_count += 1
    elif self.moving_right:
      self.facing_right = True
      win.blit(self.walk_right[self.walk_count], self.position)
      self.walk_count += 1
    elif self.facing_right:
      win.blit(self.idle_right, self.position)
    else:
      win.blit(self.idle_left, self.position)

    self.move_hitbox()
    pygame.draw.rect(win, (255, 0, 0), (self.hitbox[0], self.hitbox[1] - 20, 50, 10))
    pygame.draw.rect(win, (0, 255, 0), (self.hitbox[0], self.hitbox[1] - 20, 50 - ((50 / self.max_health) * (self.max_health - self.health)), 10))

  def take_damage(self):
    if self.health > 1:
      self.health -= 1
    else: 
      self.dead = True

  def move_left(self):
    self.x -= self.velocity
    self.moving_right = False
    self.moving_left = True

  def move_right(self):
    self.x += self.velocity
    self.moving_right = True
    self.moving_left = False