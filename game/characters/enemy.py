from . import Character
from ..animations import warrior_walkright, warrior_walkleft

class Enemy(Character):
  def __init__(self, x, y, width, height, end):
    super().__init__(x, y, width, height)
    self.walk_right = warrior_walkright()
    self.walk_left = warrior_walkleft()
    self.velocity = -3
    self.end = end
    self.path = [x, end]
    self.moving_left = True
    self.moving_right = False
    self.max_health = 10
    self.health = 10

  def move_hitbox(self):
    self.hitbox = (self.x + 20, self.y, 28, 60)

  def move(self):
    start, finish = self.path
    if self.velocity > 0:
      self.moving_right = True
      self.moving_left = False
      if (self.x + self.velocity) < finish:
        self.x += self.velocity
      else:
        self.turn_around()
    else:
      self.moving_right = False
      self.moving_left = True
      if (self.x - self.velocity) > start:
        self.x += self.velocity
      else:
        self.turn_around()

    if not self.dead:
      self.render()

  def turn_around(self):
    self.velocity *= -1
    self.walk_count = 0